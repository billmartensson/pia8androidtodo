package se.magictechnology.pia8todo


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.InputType
import android.util.Log
import android.view.*
import android.widget.EditText
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.fragment_todo_lists.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class TodoListsFragment : Fragment() {

    lateinit var listsadapter : Todolists_adapter

    lateinit var viewModel : TodoListsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_todo_lists, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(TodoListsViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()

        listsadapter = Todolists_adapter()

        todolists_recview.apply {
            layoutManager = LinearLayoutManager(this@TodoListsFragment.context)

            adapter = listsadapter
        }

        todolists_fab.setOnClickListener {
            Log.i("pia8app", "KLICK PÅ FAB")

            //viewModel.addTodoList("Ny sak på listan")

            val todoAlert = android.app.AlertDialog.Builder(this@TodoListsFragment.context)
            todoAlert.setTitle("TODO List")
            todoAlert.setMessage("Name of todo list")
            val todoalertEdittext = EditText(this@TodoListsFragment.context)
            todoalertEdittext.inputType = InputType.TYPE_CLASS_TEXT
            todoAlert.setView(todoalertEdittext)

            todoAlert.setNegativeButton("Cancel") { dialogInterface: DialogInterface, _ ->

            }

            todoAlert.setPositiveButton("Add") { dialogInterface: DialogInterface, _ ->
                var thetext = todoalertEdittext.text.toString()
                viewModel.addTodoList(thetext)
            }
            todoAlert.show()



        }

        viewModel.getTodolists().observe(this, Observer<List<Todolist>> {changedTodolist ->
            listsadapter.updateLists(changedTodolist!!)
        })

    }

    /*
    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater!!.inflate(R.menu.todolists, menu)

    }
    */
    /*
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> {
                Log.i("pia8app", "KLICK SETTINGS")
                return true
            }
            R.id.action_morestuff -> {
                Log.i("pia8app", "KLICK MORESTUFF")
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
    */

}
