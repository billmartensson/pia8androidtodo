package se.magictechnology.pia8todo


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_list_detail.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ListDetailFragment : Fragment() {

    lateinit var listdetailadapter : Todolistdetail_adapter

    lateinit var viewModel : ListDetailViewModel

    lateinit var currentTodolist : Todolist

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_detail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(ListDetailViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()

        currentTodolist = arguments!!.getSerializable("currentTodolist") as Todolist

        viewModel.currenttodolist = currentTodolist

        Log.i("pia8app", currentTodolist.listname)

        listdetailadapter = Todolistdetail_adapter()

        listdetail_recview.apply {
            layoutManager = LinearLayoutManager(this@ListDetailFragment.context)
            adapter = listdetailadapter
        }

        viewModel.getTodoitems().observe(this, Observer<List<Todoitem>> {changedTodoitems ->
            listdetailadapter.updateItems(changedTodoitems!!)
        })
    }

}
