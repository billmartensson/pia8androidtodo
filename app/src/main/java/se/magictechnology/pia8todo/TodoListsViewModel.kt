package se.magictechnology.pia8todo

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class TodoListsViewModel : ViewModel()
{
    private lateinit var todolists : MutableLiveData<List<Todolist>>

    fun getTodolists() : LiveData<List<Todolist>>
    {
        if (!::todolists.isInitialized)
        {
            todolists = MutableLiveData()
            loadTodo()
        }
        return todolists
    }

    fun addTodoList(todolistName : String)
    {
        if(todolistName == "")
        {
            return
        }
        var addingTodo = Todolist(todolistName)

        var database = FirebaseDatabase.getInstance().reference
        val auth = FirebaseAuth.getInstance()
        database.child("todo").child(auth.currentUser!!.uid).child("lists").push().setValue(addingTodo).addOnSuccessListener {
            loadTodo()
        }

        /*
        var changingList = todolists.value!! as MutableList<Todolist>

        changingList.add(Todolist(todolistName))

        todolists.value = changingList
        */
    }


    private fun loadTodo()
    {
        /*
        var loadedLists = mutableListOf<Todolist>()
        loadedLists.add(Todolist("Handla"))
        loadedLists.add(Todolist("Köpa"))
        loadedLists.add(Todolist("Fixa"))

        todolists.value = loadedLists
        */

        var database = FirebaseDatabase.getInstance().reference
        val auth = FirebaseAuth.getInstance()


        var fbGetFrom = database.child("todo").child(auth.currentUser!!.uid).child("lists")


        val fbListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                Log.i("pia8app", "FB onDataChange")
                var gettingTodo = mutableListOf<Todolist>()
                for(todoSnapshot in dataSnapshot.children)
                {
                    Log.i("pia8app", "FB loop data")
                    val fblist = todoSnapshot.getValue(Todolist::class.java)!!
                    fblist.fbkey = todoSnapshot.key
                    gettingTodo.add(fblist)
                }


                todolists.value = gettingTodo
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("pia8app", "loadPost:onCancelled", databaseError.toException())
                // ...
            }
        }
        fbGetFrom.addListenerForSingleValueEvent(fbListener)

    }
}