package se.magictechnology.pia8todo

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.todolists_row.view.*

class Todolists_adapter() : RecyclerView.Adapter<Todolists_adapter.TodolistViewHolder>() {

    var todolists : List<Todolist>? = null

    fun updateLists(newlists : List<Todolist>)
    {
        todolists = newlists
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, rownumber: Int): Todolists_adapter.TodolistViewHolder {
        var theholder = TodolistViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.todolists_row, parent, false))
        theholder.theadapter = this
        return theholder
    }

    override fun getItemCount(): Int {
        if (todolists == null)
        {
            return 0
        }
        return todolists!!.size
    }

    override fun onBindViewHolder(holder: TodolistViewHolder, rownumber: Int) {
        holder.listname_textview.text = todolists!!.get(rownumber).listname
    }

    class TodolistViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        lateinit var theadapter : Todolists_adapter

        val listname_textview = view.listname_textView

        init {
            view.setOnClickListener {
                Log.i("pia8app", "KLICKAT PÅ RADEN " + adapterPosition.toString())
                /*
                Toast.makeText(view.context, "KLICK PÅ RAD " + theadapter.people.get(adapterPosition) , Toast.LENGTH_SHORT).show()

                val goDetailIntent = Intent(view.context, PersonDetailActivity::class.java)

                goDetailIntent.putExtra("person", theadapter.people.get(adapterPosition))

                view.context.startActivity(goDetailIntent)
                */

                var navbundle = Bundle()
                navbundle.putSerializable("currentTodolist", theadapter.todolists!!.get(adapterPosition))


                view.findNavController().navigate(R.id.action_todoListsFragment_to_listDetailFragment, navbundle)

            }
        }
    }

}