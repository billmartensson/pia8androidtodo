package se.magictechnology.pia8todo

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ListDetailViewModel : ViewModel()
{
    private lateinit var todoitems : MutableLiveData<List<Todoitem>>

    var currenttodolist : Todolist? = null

    fun getTodoitems() : LiveData<List<Todoitem>>
    {
        if (!::todoitems.isInitialized)
        {
            todoitems = MutableLiveData()
            loadItems()
        }
        return todoitems
    }

    fun loadItems()
    {
        /*
        var tempItems = mutableListOf<Todoitem>()
        tempItems.add(Todoitem("Test 1"))
        tempItems.add(Todoitem("Test 2"))
        tempItems.add(Todoitem("Test 3"))

        todoitems.value = tempItems
        */

        var database = FirebaseDatabase.getInstance().reference
        val auth = FirebaseAuth.getInstance()


        var fbGetFrom = database.child("todo").child(auth.currentUser!!.uid).child("lists").child(currenttodolist!!.fbkey!!).child("listitems")


        val fbListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                Log.i("pia8app", "FB onDataChange")
                var gettingItems = mutableListOf<Todoitem>()
                for(todoSnapshot in dataSnapshot.children)
                {
                    Log.i("pia8app", "FB loop item data")
                    val fbitem = todoSnapshot.getValue(Todoitem::class.java)!!
                    fbitem.fbkey = todoSnapshot.key
                    gettingItems.add(fbitem)
                }


                todoitems.value = gettingItems
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("pia8app", "loadPost:onCancelled", databaseError.toException())
                // ...
            }
        }
        fbGetFrom.addListenerForSingleValueEvent(fbListener)
    }
}