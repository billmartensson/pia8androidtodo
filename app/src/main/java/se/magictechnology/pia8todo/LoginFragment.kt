package se.magictechnology.pia8todo


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_login.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class LoginFragment : Fragment() {

    lateinit var auth : FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }


    override fun onStart() {
        super.onStart()

        auth = FirebaseAuth.getInstance()

        signup_button.setOnClickListener {
            auth.createUserWithEmailAndPassword(loginusername_editText.text.toString(), loginpassword_editText.text.toString())
                .addOnCompleteListener(this.activity!!) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("pia8app", "createUserWithEmail:success")
                        val user = auth.currentUser

                        // OK. STÄNG LOGIN. VISA LISTOR
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("pia8app", "createUserWithEmail:failure", task.exception)
                        Toast.makeText(this.context, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()

                    }

                }
        }

        login_button.setOnClickListener {
            Log.i("pia8app", "LETS LOGIN")

            auth.signInWithEmailAndPassword(loginusername_editText.text.toString(), loginpassword_editText.text.toString())
                .addOnCompleteListener(this.activity!!) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("pia8app", "signInWithEmail:success")

                        var mainact = this.activity as MainActivity
                        mainact.checkLogin()
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("pia8app", "signInWithEmail:failure", task.exception)
                        Toast.makeText(this.context, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()

                    }

                }
        }
    }

}
