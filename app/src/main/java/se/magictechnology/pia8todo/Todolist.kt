package se.magictechnology.pia8todo

import java.io.Serializable

data class Todolist(var listname : String? = "") : Serializable
{
    var fbkey : String? = ""
}