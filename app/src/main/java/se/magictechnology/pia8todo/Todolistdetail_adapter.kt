package se.magictechnology.pia8todo

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.todoitems_row.view.*
import kotlinx.android.synthetic.main.todolists_row.view.*

class Todolistdetail_adapter() : RecyclerView.Adapter<Todolistdetail_adapter.TodoitemViewHolder>() {

    var items : List<Todoitem>? = null

    fun updateItems(newitems : List<Todoitem>)
    {
        items = newitems
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, rownumber: Int): Todolistdetail_adapter.TodoitemViewHolder {
        var theholder = Todolistdetail_adapter.TodoitemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.todoitems_row,
                parent,
                false
            )
        )
        theholder.theadapter = this
        return theholder
    }

    override fun getItemCount(): Int {
        if(items == null)
        {
            return 0
        }
        return items!!.size
    }

    override fun onBindViewHolder(holder: Todolistdetail_adapter.TodoitemViewHolder, rownumber: Int) {
        holder.itemname_textView.text = items!!.get(rownumber).itemname
    }

    class TodoitemViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        lateinit var theadapter : Todolistdetail_adapter

        val itemname_textView = view.itemname_textView

        init {
            view.setOnClickListener {
                Log.i("pia8app", "KLICKAT PÅ RADEN " + adapterPosition.toString())
                /*
                Toast.makeText(view.context, "KLICK PÅ RAD " + theadapter.people.get(adapterPosition) , Toast.LENGTH_SHORT).show()

                val goDetailIntent = Intent(view.context, PersonDetailActivity::class.java)

                goDetailIntent.putExtra("person", theadapter.people.get(adapterPosition))

                view.context.startActivity(goDetailIntent)
                */

                view.findNavController().navigate(R.id.action_listDetailFragment_to_itemDetailFragment)

            }
        }
    }

}