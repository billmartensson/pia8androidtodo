package se.magictechnology.pia8todo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.navigation.fragment.NavHostFragment
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        checkLogin()

        /*
        val fragTransaction = supportFragmentManager.beginTransaction()
        fragTransaction.add(R.id.main_framelayout, StartFragment())
        fragTransaction.commit()
        */
    }

    fun checkLogin()
    {
        Log.i("pia8app", "LETS CHECK LOGIN")
        invalidateOptionsMenu()

        val auth = FirebaseAuth.getInstance()

        if(auth.currentUser != null)
        {
            Log.i("pia8app", "LOGGED IN AS "+auth.currentUser!!.uid)
            val finalHost = NavHostFragment.create(R.navigation.nav_graph)
            supportFragmentManager.beginTransaction()
                .replace(R.id.main_framelayout, finalHost)
                .setPrimaryNavigationFragment(finalHost) // this is the equivalent to app:defaultNavHost="true"
                .commit()
        } else {
            val finalHost = NavHostFragment.create(R.navigation.login_nav_graph)
            supportFragmentManager.beginTransaction()
                .replace(R.id.main_framelayout, finalHost)
                .setPrimaryNavigationFragment(finalHost) // this is the equivalent to app:defaultNavHost="true"
                .commit()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val auth = FirebaseAuth.getInstance()

        if(auth.currentUser != null)
        {
            menuInflater.inflate(R.menu.todolists, menu)
        } else {
            menuInflater.inflate(R.menu.login, menu)
        }



        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.logoutuser -> {
                Log.i("pia8app", "LETS LOGOUT")

                val auth = FirebaseAuth.getInstance()
                auth.signOut()


                checkLogin()

                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

}
